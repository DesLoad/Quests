﻿using System;
using System.Collections.Generic;

namespace Quest_3
{
    class Program
    {
        #region Data
        //Archer
        static private int hp_Archer = 600;
        static private int dmg_Archer = 10;
        static private int evade_Chance_Archer = 40;
        static private int as_Archer = 2; //ударов за тик

        //Soldier
        static private int hp_Soldier = 800;
        static private int dmg_Soldier = 25;
        static private int ms_Soldier = 10; //расстояние за 1 тик (ms - move speed)
        static private int as_Soldier = 1;  //ударов за 1 тик (ms - move speed)
        static private int block_Chance_Soldier = 50;
        static private int block_Value_Soldier = 7;

        //World data
        static private int start_Distance_WData = 60;
        static private int distance;

        #endregion

        static void Main()
        {
            List<int> turnsForWinsArcher = new List<int>();
            List<int> turnsForWinsSoldier = new List<int>();
            Random chance = new Random();
            int turn = 0;
            bool stop = false;
            int[] win = new int[2];// [0]- Soldier; [1] - Archer


            distance = start_Distance_WData;
            for (int games = 0; games < 1000; games++)
            {
                turn = 0;
                int hpArcher = hp_Archer;
                int hpSoldier = hp_Soldier;
                distance = start_Distance_WData;
                stop = false;

                while (stop == false)
                {
                    turn++;
                    //Console.WriteLine("Turn: " + turn);
                    if (distance != 0)
                    {
                        distance -= ms_Soldier;
                    }
                    else
                    {
                        if (chance.Next(100) >= evade_Chance_Archer)
                        {
                            //Nothing;
                        }
                        else
                        {
                            hpArcher -= dmg_Soldier;
                        }
                    }

                    for (int i = 0; i != as_Archer; i++)
                    {
                        if (chance.Next(100) >= block_Chance_Soldier)
                        {
                            hpSoldier = hpSoldier - dmg_Archer + block_Value_Soldier;
                        }
                        else
                        {
                            hpSoldier = hpSoldier - dmg_Archer;
                        }
                    }
                    if (hpArcher <= 0)
                    {
                        win[0]++;
                        turnsForWinsSoldier.Add(turn);
                        Console.WriteLine("Turns: " + turn + "(Soldier WINS)");
                        stop = true;
                    }
                    else if (hpSoldier <= 0)
                    {
                        win[1]++;
                        turnsForWinsArcher.Add(turn);
                        Console.WriteLine("Turns: " + turn + "(Archer WINS)");
                        stop = true;
                    }
                }
            }
            float avgTurnsArcher = 0;
            for (int i = 0; i < turnsForWinsArcher.Count; i++)
            {
                avgTurnsArcher += turnsForWinsArcher[i];
            }
            float avgTurnsSoldier = 0;
            for (int i = 0; i < turnsForWinsSoldier.Count; i++)
            {
                avgTurnsSoldier += turnsForWinsSoldier[i];
            }
            avgTurnsArcher = avgTurnsArcher / turnsForWinsArcher.Count;
            avgTurnsSoldier = avgTurnsSoldier / turnsForWinsSoldier.Count;
            Console.WriteLine("Soldier = " + win[0] + " || Archer = " + win[1] + " || AvgTurnsSoldier = " + avgTurnsSoldier + " || AvgTurnsArcher = " + avgTurnsArcher);
        }
    }
}
